import { TestBed } from '@angular/core/testing';

import { WhoWeAreService } from './who-we-are.service';

describe('WhoWeAreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WhoWeAreService = TestBed.get(WhoWeAreService);
    expect(service).toBeTruthy();
  });
});
