import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WhoWeAreService {
  whoService: string[];

  constructor() { }

  getCurses(){
    return ['Java',['Spring', 'JSF', 'Hibernate', 'JPA', 'CDI'], ['Python',['Django']], 'HTML', 'CSS', ['JavaScript',['JQuery','Ajax']],'Angular', 'MySQL'];
  }
}
