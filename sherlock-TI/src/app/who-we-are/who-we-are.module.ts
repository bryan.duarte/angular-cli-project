import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WhoWeAreComponent } from './who-we-are.component';
import { TeamComponent } from './team/team.component';
@NgModule({
  declarations: [
    WhoWeAreComponent,
    TeamComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    WhoWeAreComponent,
  ]
})
export class WhoWeAreModule { }
