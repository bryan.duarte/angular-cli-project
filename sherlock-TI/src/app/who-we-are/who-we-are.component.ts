import { WhoWeAreService } from './../services/who-we-are.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-who-we-are',
  templateUrl: './who-we-are.component.html',
  styleUrls: ['./who-we-are.component.css']
})
export class WhoWeAreComponent implements OnInit {

  name: string;
  list: any[];

  constructor(whoService: WhoWeAreService) {
    this.name = 'Bryan Duarte';
    this.list = whoService.getCurses();
   }



  ngOnInit() {
  }

}
