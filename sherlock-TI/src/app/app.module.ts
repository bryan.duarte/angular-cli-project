import { WhoWeAreModule } from './who-we-are/who-we-are.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WhoWeAreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
